package com.example.transactionenrichment.transactions;

import com.example.transactionenrichment.infrastructure.dtos.TransactionOutgoing;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import static com.example.transactionenrichment.config.TestSamples.*;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class TransactionsWithPaginationTest {

    TransactionFacadeConfig config = new TransactionFacadeConfig();
    TransactionFacade facade = config.createFacade();

    @Test
    void shouldReturnPageOfTransactionsWithinGivenPeriodOfTime() {
        //given
        TransactionOutgoing sampleDto = facade.enrichTransactionWithSumOfPrices(sampleTransaction());
        TransactionOutgoing anotherSampleDto = facade.enrichTransactionWithSumOfPrices(anotherSampleTransaction());
        facade.enrichTransactionWithSumOfPrices(latestSampleTransaction());
        //when
        Page<TransactionOutgoing> outgoingTransactionsPage = facade.findTransactionsBetweenDates(INSTANT_IN_SAMPLE_TRANSACTION, INSTANT_IN_ANOTHER_SAMPLE_TRANSACTION, PageRequest.of(0, 3));
        //then
        assertThat(outgoingTransactionsPage.getContent()).asList()
                .hasSize(2)
                .containsExactlyInAnyOrder(sampleDto, anotherSampleDto);
    }
}
