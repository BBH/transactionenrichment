package com.example.transactionenrichment.transactions;

import com.example.transactionenrichment.infrastructure.dtos.ProductDto;
import com.example.transactionenrichment.infrastructure.dtos.TransactionIncoming;
import com.example.transactionenrichment.infrastructure.dtos.TransactionOutgoing;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static com.example.transactionenrichment.config.TestSupplementaryMethods.*;
import static com.example.transactionenrichment.config.TestSamples.*;
import static com.example.transactionenrichment.config.TestSamples.sampleTransaction;
import static org.assertj.core.api.Assertions.assertThat;

public class TransactionEnrichmentTest {

    TransactionFacadeConfig config = new TransactionFacadeConfig();
    TransactionFacade facade = config.createFacade();

    @Test
    void shouldEnrichIncomingTransactionEventWithSumOfPricesOfProducts() {
        //given
        TransactionIncoming transactionEvent = sampleTransaction();
        //when
        facade.enrichTransactionWithSumOfPrices(transactionEvent);
        //then
        Optional<TransactionOutgoing> optionalResult = facade.findById(transactionEvent.getId());
        assertThat(optionalResult.isPresent()).isTrue();
        //and then
        TransactionOutgoing result = optionalResult.get();
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(result.getId()).isEqualTo(transactionEvent.getId());
            softly.assertThat(result.getSaleDate()).isEqualTo(transactionEvent.getSaleDate());
            softly.assertThat(result.getProducts())
                    .extracting(ProductDto::getName, ProductDto::getPrice)
                    .containsExactlyInAnyOrderElementsOf(listOfProductEntitiesIn(transactionEvent));
            softly.assertThat(result.getSumOfProductPrices()).isEqualTo(SUM_OF_PRICES_OF_PRODUCTS_IN_SAMPLE_TRANSACTION);
        });
    }


}
