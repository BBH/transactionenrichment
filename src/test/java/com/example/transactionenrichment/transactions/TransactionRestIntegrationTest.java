package com.example.transactionenrichment.transactions;

import com.example.transactionenrichment.config.IntegrationTest;
import com.example.transactionenrichment.config.TestSupplementaryMethods;
import com.example.transactionenrichment.infrastructure.dtos.ProductDto;
import com.example.transactionenrichment.infrastructure.dtos.TransactionIncoming;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.MediaTypes;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import static com.example.transactionenrichment.config.TestSamples.*;
import static com.example.transactionenrichment.config.TestSupplementaryMethods.listOfProductEntitiesIn;
import static com.example.transactionenrichment.transactions.infrastructure.kafkalistener.TransactionEventListenerConfig.TOPIC;
import static net.javacrumbs.jsonunit.assertj.JsonAssertions.assertThatJson;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.testcontainers.shaded.org.awaitility.Awaitility.await;

public class TransactionRestIntegrationTest extends IntegrationTest {

    @Autowired
    KafkaTemplate<String, TransactionIncoming> kafkaTemplate;

    @Autowired
    MockMvc mockMvc;

    @Autowired
    TransactionFacade facade;

    @Autowired
    ObjectMapper objectMapper;

    @Test
    void shouldReturnEnrichedTransactionWithGivenId() throws Exception {
        //when
        TransactionIncoming transactionEvent = sampleTransaction();
        String transactionId = transactionEvent.getId();
        TransactionIncoming anotherTransactionEvent = anotherSampleTransaction();
        kafkaTemplate.send(TOPIC, transactionId, transactionEvent);
        kafkaTemplate.send(TOPIC, anotherTransactionEvent.getId(), anotherTransactionEvent);
        //then
        AtomicReference<String> result = new AtomicReference<>("");
        await().untilAsserted(() -> {
                    result.set(getResponseFor("/transactions/" + transactionId));
                    assertThatJson(result.get())
                            .and(content -> content.node("_links.self.href").isString().contains(transactionId))
                            .and(
                                    content -> content.node("id").isString().isEqualTo(transactionId),
                                    content -> content.node("sumOfProductPrices").isNumber().isEqualByComparingTo(SUM_OF_PRICES_OF_PRODUCTS_IN_SAMPLE_TRANSACTION),
                                    content -> content.node("saleDate").isString().contains(DAY_HOUR_IN_SAMPLE_TRANSACTION),
                                    content -> content.node("products").isArray().hasSize(transactionEvent.getProducts().size())
                            );
                }
        );
        //and then
        assertThat(getProductsFrom(result))
                .extracting(ProductDto::getName, productDto -> TestSupplementaryMethods.getBigDecimalOf(productDto.getPrice().doubleValue()))
                .containsExactlyInAnyOrderElementsOf(listOfProductEntitiesIn(transactionEvent));
    }

    @Test
    void shouldReturn404IfTransactionNotFound() throws Exception {
        //expect
        mockMvc
                .perform(get("/transactions/invalidTransactionId"))
                .andExpect(status().isNotFound());
    }


    @Test
    void shouldReturnPaginatedResultOfGettingTransactionsWithinRangeOfDates() {
        //when
        fourTransactionEventsArePublished();
        //then
        AtomicReference<String> result = new AtomicReference<>("");
        await().untilAsserted(() -> {
                    result.set(getResponseFor("/transactions?page=1&size=1&dateFrom=" + INSTANT_IN_SAMPLE_TRANSACTION + "&dateTo=" + INSTANT_IN_ANOTHER_SAMPLE_TRANSACTION));
                    assertThatJson(result.get())
                            .and(
                                    content -> content.node("page.totalPages").isNumber().isGreaterThan(BigDecimal.valueOf(2)),
                                    content -> content.node("_links.prev.href").isString().contains("page=0"),
                                    content -> content.node("_links.self.href").isString().contains("page=1"),
                                    content -> content.node("_links.next.href").isString().contains("page=2")
                            ).and(
                                    content -> content.node("_embedded.transactions[0].saleDate").isString().containsAnyOf(DAY_HOUR_IN_SAMPLE_TRANSACTION, DAY_HOUR_IN_MID_TRANSACTION, DAY_HOUR_IN_ANOTHER_TRANSACTION)
                            );
                }
        );

    }

    private void fourTransactionEventsArePublished() {
        TransactionIncoming sampleTransaction = sampleTransaction();
        TransactionIncoming midTransaction = midSampleTransaction();
        TransactionIncoming anotherTransaction = anotherSampleTransaction();
        TransactionIncoming latestSampleTransaction = latestSampleTransaction();
        kafkaTemplate.send(TOPIC, sampleTransaction.getId(), sampleTransaction);
        kafkaTemplate.send(TOPIC, midTransaction.getId(), midTransaction);
        kafkaTemplate.send(TOPIC, anotherTransaction.getId(), anotherTransaction);
        kafkaTemplate.send(TOPIC, latestSampleTransaction.getId(), latestSampleTransaction);
    }


    private String getResponseFor(String uriTemplate) throws Exception {
        return mockMvc
                .perform(get(uriTemplate))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaTypes.HAL_JSON))
                .andReturn()
                .getResponse()
                .getContentAsString();
    }

    private List<ProductDto> getProductsFrom(AtomicReference<String> result) throws JsonProcessingException {
        String jsonArrayOfDtos = objectMapper.readTree(result.get()).at("/products").toString();
        return objectMapper.readValue(jsonArrayOfDtos, new TypeReference<>() {
        });
    }
}

