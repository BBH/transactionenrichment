package com.example.transactionenrichment.transactions;

import com.example.transactionenrichment.config.IntegrationTest;
import com.example.transactionenrichment.infrastructure.dtos.ProductDto;
import com.example.transactionenrichment.infrastructure.dtos.TransactionIncoming;
import com.example.transactionenrichment.infrastructure.dtos.TransactionOutgoing;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.temporal.ChronoUnit;
import java.util.Optional;

import static com.example.transactionenrichment.config.TestSupplementaryMethods.listOfProductEntitiesIn;
import static com.example.transactionenrichment.config.TestSamples.*;
import static com.example.transactionenrichment.config.TestSamples.anotherSampleTransaction;
import static com.example.transactionenrichment.config.TestSamples.sampleTransaction;
import static org.assertj.core.api.Assertions.assertThat;


class TransactionEnrichmentIntegrationTest extends IntegrationTest {

    @Autowired
    TransactionFacade facade;

    @Test
    void shouldFindCorrectTransaction() {
        //given
        TransactionIncoming transactionEvent = sampleTransaction();
        facade.enrichTransactionWithSumOfPrices(transactionEvent);
        facade.enrichTransactionWithSumOfPrices(anotherSampleTransaction());
        //when
        Optional<TransactionOutgoing> optionalResult = facade.findById(transactionEvent.getId());
        //then
        assertThat(optionalResult.isPresent()).isTrue();
        TransactionOutgoing result = optionalResult.get();
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(result.getId()).isEqualTo(transactionEvent.getId());
            softly.assertThat(result.getSaleDate().truncatedTo(ChronoUnit.MILLIS)).isEqualTo(transactionEvent.getSaleDate().truncatedTo(ChronoUnit.MILLIS));
            softly.assertThat(result.getProducts())
                    .extracting(ProductDto::getName, ProductDto::getPrice)
                    .containsExactlyInAnyOrderElementsOf(listOfProductEntitiesIn(transactionEvent));
            softly.assertThat(result.getSumOfProductPrices()).isEqualTo(SUM_OF_PRICES_OF_PRODUCTS_IN_SAMPLE_TRANSACTION);
        });
    }
}
