package com.example.transactionenrichment.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.config.TopicBuilder;
import org.testcontainers.containers.KafkaContainer;
import org.testcontainers.utility.DockerImageName;

import static com.example.transactionenrichment.Profiles.INTEGRATION_TEST;
import static com.example.transactionenrichment.transactions.infrastructure.kafkalistener.TransactionEventListenerConfig.TOPIC;


@Configuration
@Profile(INTEGRATION_TEST)
public class KafkaIntegrationTestConfig {

    public static final String TESTCONTAINERS_DOCKER_IMAGE = "confluentinc/cp-kafka:6.2.1";
    public static final int NUMBER_OF_PARTITIONS = 4;

    @Bean
    String kafkaBootstrapServer() {
        KafkaContainer kafkaContainer = new KafkaContainer(DockerImageName.parse(TESTCONTAINERS_DOCKER_IMAGE));
        kafkaContainer.start();
        createTopic();
        return kafkaContainer.getBootstrapServers();
    }

    private void createTopic() {
        TopicBuilder.name(TOPIC)
                .partitions(NUMBER_OF_PARTITIONS)
                .build();
    }
}
