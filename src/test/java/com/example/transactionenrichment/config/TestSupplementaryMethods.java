package com.example.transactionenrichment.config;

import com.example.transactionenrichment.infrastructure.dtos.TransactionIncoming;
import org.assertj.core.groups.Tuple;
import org.jetbrains.annotations.NotNull;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import static java.math.RoundingMode.FLOOR;
import static org.assertj.core.api.Assertions.tuple;

public class TestSupplementaryMethods {

    @NotNull
    public static List<Tuple> listOfProductEntitiesIn(TransactionIncoming transactionEvent) {
        return transactionEvent.getProducts()
                .stream()
                .map(p -> tuple(p.getName(), p.getPrice()))
                .collect(Collectors.toList());
    }

    @NotNull
    public static BigDecimal getBigDecimalOf(double price) {
        return BigDecimal.valueOf(price).setScale(2, FLOOR);
    }
}
