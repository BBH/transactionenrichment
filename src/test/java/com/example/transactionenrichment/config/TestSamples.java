package com.example.transactionenrichment.config;

import com.example.transactionenrichment.infrastructure.dtos.ProductDto;
import com.example.transactionenrichment.infrastructure.dtos.TransactionIncoming;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;
import java.util.UUID;

import static com.example.transactionenrichment.config.TestSupplementaryMethods.getBigDecimalOf;
import static com.example.transactionenrichment.infrastructure.dtos.TransactionIncoming.builder;

public class TestSamples {

    public static final BigDecimal SUM_OF_PRICES_OF_PRODUCTS_IN_SAMPLE_TRANSACTION = getBigDecimalOf(3.5);
    public static final String DAY_HOUR_IN_SAMPLE_TRANSACTION = "2022-01-16T18";
    public static final Instant INSTANT_IN_SAMPLE_TRANSACTION = Instant.parse(DAY_HOUR_IN_SAMPLE_TRANSACTION + ":00:00.00Z");
    public static final String DAY_HOUR_IN_ANOTHER_TRANSACTION = "2022-01-17T18";
    public static final Instant INSTANT_IN_ANOTHER_SAMPLE_TRANSACTION = Instant.parse(DAY_HOUR_IN_ANOTHER_TRANSACTION+":00:00.00Z");
    public static final String DAY_HOUR_IN_MID_TRANSACTION = "2022-01-17T12";
    private static final Instant INSTANT_IN_MID_SAMPLE_TRANSACTION = Instant.parse(DAY_HOUR_IN_MID_TRANSACTION+":00:00.00Z");
    public static final Instant INSTANT_IN_LATER_TRANSACTION = Instant.parse("2022-01-18T18:00:00.00Z");

    public static TransactionIncoming sampleTransaction() {
        return getTransactionEvent(INSTANT_IN_SAMPLE_TRANSACTION, sampleProduct("product1", getBigDecimalOf(2.5)), sampleProduct("product2", getBigDecimalOf(1)));
    }

    public static TransactionIncoming midSampleTransaction() {
        return getTransactionEvent(INSTANT_IN_MID_SAMPLE_TRANSACTION, sampleProduct("product2", getBigDecimalOf(1)), sampleProduct("product7", getBigDecimalOf(9.55)));
    }


    public static TransactionIncoming anotherSampleTransaction() {
        return getTransactionEvent(INSTANT_IN_ANOTHER_SAMPLE_TRANSACTION, sampleProduct("product3", getBigDecimalOf(2.04)), sampleProduct("product4", getBigDecimalOf(3.46)));
    }

    public static TransactionIncoming latestSampleTransaction() {
        return getTransactionEvent(INSTANT_IN_LATER_TRANSACTION, sampleProduct("product4", getBigDecimalOf(31)), sampleProduct("product5", getBigDecimalOf(69)));
    }


    private static ProductDto sampleProduct(String name, BigDecimal price) {
        return ProductDto.builder().name(name).price(price).build();
    }

    private static TransactionIncoming getTransactionEvent(Instant date, ProductDto product1, ProductDto product2) {
        List<ProductDto> productList = List.of(product1, product2);
        return builder()
                .id(UUID.randomUUID().toString())
                .saleDate(date)
                .products(productList)
                .build();
    }


}
