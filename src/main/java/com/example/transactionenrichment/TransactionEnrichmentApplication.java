package com.example.transactionenrichment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TransactionEnrichmentApplication {

    public static void main(String[] args) {
        SpringApplication.run(TransactionEnrichmentApplication.class, args);
    }

}
