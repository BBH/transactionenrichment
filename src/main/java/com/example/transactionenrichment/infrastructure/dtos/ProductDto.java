package com.example.transactionenrichment.infrastructure.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Value;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;

import java.math.BigDecimal;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

@Value
@Builder
@JsonInclude(NON_NULL)
@Relation(collectionRelation = "products")
@EqualsAndHashCode(callSuper = false)
public class ProductDto extends RepresentationModel<ProductDto> {

    String name;
    BigDecimal price;
}