package com.example.transactionenrichment.infrastructure.dtos;

import lombok.Builder;
import lombok.Value;

import java.time.Instant;
import java.util.List;

@Builder
@Value
public class TransactionIncoming {
    String id;
    Instant saleDate;
    List<ProductDto> products;
}
