package com.example.transactionenrichment.infrastructure.dtos;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Value;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;

@Value
@Builder
@JsonRootName(value = "transaction")
@Relation(collectionRelation = "transactions")
@EqualsAndHashCode(callSuper = false)
public class TransactionOutgoing extends RepresentationModel<TransactionOutgoing> {

    String id;
    Instant saleDate;
    List<ProductDto> products;
    BigDecimal sumOfProductPrices;
}
