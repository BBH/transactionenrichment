package com.example.transactionenrichment.infrastructure.hateoas;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class HateoasConfig {

    @Bean
    TransactionOutgoingAssembler transactionOutgoingAssembler(){
        return new TransactionOutgoingAssembler();
    }
}
