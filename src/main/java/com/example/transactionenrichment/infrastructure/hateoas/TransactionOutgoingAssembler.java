package com.example.transactionenrichment.infrastructure.hateoas;

import com.example.transactionenrichment.infrastructure.dtos.TransactionOutgoing;
import com.example.transactionenrichment.transactions.infrastructure.hateoasController.HateoasController;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

public class TransactionOutgoingAssembler extends RepresentationModelAssemblerSupport<TransactionOutgoing, TransactionOutgoing> {

    public TransactionOutgoingAssembler() {
        super(HateoasController.class, TransactionOutgoing.class);
    }

    @Override
    public TransactionOutgoing toModel(TransactionOutgoing dto) {
        return dto
                .add(linkTo(
                methodOn(HateoasController.class).getEnrichedTransactionWithGivenId(dto.getId()))
                .withSelfRel());
    }
}

