package com.example.transactionenrichment.infrastructure.kafka;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.*;
import org.springframework.kafka.listener.CommonErrorHandler;
import org.springframework.kafka.support.serializer.ErrorHandlingDeserializer;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.kafka.support.serializer.JsonSerializer;

import java.util.HashMap;
import java.util.Map;

import static com.example.transactionenrichment.transactions.infrastructure.kafkalistener.TransactionEventListenerConfig.GROUP_ID;
import static org.apache.kafka.clients.consumer.ConsumerConfig.*;
import static org.apache.kafka.clients.producer.ProducerConfig.BOOTSTRAP_SERVERS_CONFIG;
import static org.apache.kafka.clients.producer.ProducerConfig.*;

public interface KafkaFactories {

    static <T> ConcurrentKafkaListenerContainerFactory<String, T> kafkaListenerContainerFactory(String kafkaBootstrapServer, Class<T> aClass, ObjectMapper objectMapper, int concurrencyLevel, CommonErrorHandler errorHandler) {
        ConcurrentKafkaListenerContainerFactory<String, T> factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(kafkaConsumerFactory(kafkaBootstrapServer, aClass, objectMapper));
        factory.setConcurrency(concurrencyLevel);
        factory.setCommonErrorHandler(errorHandler);
        return factory;
    }

    static <T> ConsumerFactory<String, T> kafkaConsumerFactory(String kafkaBoostrapSever, Class<T> aClass, ObjectMapper objectMapper) {
        Map<String, Object> properties = new HashMap<>();
        properties.put(BOOTSTRAP_SERVERS_CONFIG, kafkaBoostrapSever);
        properties.put(GROUP_ID_CONFIG, GROUP_ID);
        properties.put(ENABLE_AUTO_COMMIT_CONFIG, Boolean.TRUE);
        properties.put(KEY_DESERIALIZER_CLASS_CONFIG, ErrorHandlingDeserializer.class);
        properties.put(VALUE_DESERIALIZER_CLASS_CONFIG, ErrorHandlingDeserializer.class);
        properties.put(JsonDeserializer.TRUSTED_PACKAGES, "*");
        properties.put(AUTO_OFFSET_RESET_CONFIG, "earliest");
        return new DefaultKafkaConsumerFactory<>(properties,
                new StringDeserializer(),
                new JsonDeserializer<>(aClass, objectMapper));
    }

    static <T> KafkaTemplate<String, T> kafkaTemplate(String kafkaBoostrapServer, ObjectMapper objectMapper) {
        return new KafkaTemplate<>(kafkaProducerFactory(kafkaBoostrapServer, objectMapper));
    }

    static <V> ProducerFactory<String, V> kafkaProducerFactory(String kafkaBoostrapServer, ObjectMapper objectMapper) {
        Map<String, Object> properties = new HashMap<>();
        properties.put(BOOTSTRAP_SERVERS_CONFIG, kafkaBoostrapServer);
        properties.put(KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        properties.put(VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);
        return new DefaultKafkaProducerFactory<>(properties,
                new StringSerializer(),
                new JsonSerializer<>(objectMapper));
    }
}
