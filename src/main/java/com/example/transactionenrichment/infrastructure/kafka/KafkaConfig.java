package com.example.transactionenrichment.infrastructure.kafka;

import com.example.transactionenrichment.infrastructure.dtos.TransactionIncoming;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.core.KafkaTemplate;

import static com.example.transactionenrichment.Profiles.NOT_INTEGRATION_TEST;
import static com.example.transactionenrichment.infrastructure.kafka.KafkaFactories.kafkaTemplate;

@Configuration
@EnableKafka
class KafkaConfig {

    @Bean
    KafkaTemplate<String, TransactionIncoming> transactionEventKafkaTemplate(String kafkaBootstrapServer, ObjectMapper objectMapper) {
        return kafkaTemplate(kafkaBootstrapServer, objectMapper);
    }

    @Bean
    @Profile(NOT_INTEGRATION_TEST)
    String kafkaBoostrapServer(@Value("${kafka.boostrap-server}") String boostrapServer) {
        return boostrapServer;
    }
}