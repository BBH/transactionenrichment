package com.example.transactionenrichment.transactions.infrastructure.kafkalistener;

import com.example.transactionenrichment.infrastructure.dtos.TransactionIncoming;
import com.example.transactionenrichment.transactions.TransactionFacade;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.listener.DefaultErrorHandler;

import static com.example.transactionenrichment.infrastructure.kafka.KafkaFactories.kafkaListenerContainerFactory;

@Configuration
@EnableKafka
public class TransactionEventListenerConfig {

    public static final String TRANSACTIONS_LISTENER_CONTAINER_FACTORY = "TransactionsListenerContainerFactory";
    public static final String TOPIC = "transactions";
    public static final int CONCURRENCY_LEVEL = 4;
    public static final String GROUP_ID = "kafkaListener";

    @Bean
    TransactionEventListener transactionEventListener(TransactionFacade facade) {
        return new TransactionEventListener(facade);
    }

    @Bean(TRANSACTIONS_LISTENER_CONTAINER_FACTORY)
    ConcurrentKafkaListenerContainerFactory<String, TransactionIncoming> transactionEventListenerContainerFactory(String kafkaBootstrapServer, ObjectMapper objectMapper) {
        return kafkaListenerContainerFactory(kafkaBootstrapServer, TransactionIncoming.class, objectMapper, CONCURRENCY_LEVEL, new DefaultErrorHandler());
    }
}


