package com.example.transactionenrichment.transactions.infrastructure.hateoasController;

import com.example.transactionenrichment.infrastructure.dtos.TransactionOutgoing;
import com.example.transactionenrichment.infrastructure.hateoas.TransactionOutgoingAssembler;
import com.example.transactionenrichment.transactions.TransactionFacade;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.Instant;

@RestController
@RequiredArgsConstructor
public class HateoasController {

    private final TransactionFacade facade;
    private final TransactionOutgoingAssembler assembler;
    private final PagedResourcesAssembler<TransactionOutgoing> pagedResourcesAssembler;

    @GetMapping("/transactions/{transactionId}")
    public ResponseEntity<TransactionOutgoing> getEnrichedTransactionWithGivenId(@PathVariable String transactionId) {
        return facade.findById(transactionId)
                .map(assembler::toModel)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @GetMapping("/transactions")
    public ResponseEntity<PagedModel<TransactionOutgoing>> getTransactionsBetweenDates(@RequestParam(value = "dateFrom", defaultValue = "1970-01-01T01:00:00Z")
                                                                                       @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) Instant from,
                                                                                       @RequestParam(value = "dateTo", defaultValue = "2050-01-01T00:00:00Z")
                                                                                       @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) Instant to,
                                                                                       Pageable pageable) {
        Page<TransactionOutgoing> transactionsBetweenDates = facade.findTransactionsBetweenDates(from, to, pageable);
        PagedModel<TransactionOutgoing> outgoingTransactions = pagedResourcesAssembler.toModel(transactionsBetweenDates, assembler);
        return new ResponseEntity<>(outgoingTransactions, HttpStatus.OK);
    }
}
