package com.example.transactionenrichment.transactions.infrastructure.kafkalistener;

import com.example.transactionenrichment.infrastructure.dtos.TransactionIncoming;
import com.example.transactionenrichment.transactions.TransactionFacade;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaHandler;
import org.springframework.kafka.annotation.KafkaListener;


@Slf4j
@RequiredArgsConstructor
@KafkaListener(topics = {TransactionEventListenerConfig.TOPIC}, groupId = TransactionEventListenerConfig.GROUP_ID, containerFactory = TransactionEventListenerConfig.TRANSACTIONS_LISTENER_CONTAINER_FACTORY)
public class TransactionEventListener {

    private final TransactionFacade facade;

    @KafkaHandler
    public void receive(TransactionIncoming transactionEvent) {
        facade.enrichTransactionWithSumOfPrices(transactionEvent);
        log.info("Event received: {}", transactionEvent);
    }
}
