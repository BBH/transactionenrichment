package com.example.transactionenrichment.transactions;

import com.example.transactionenrichment.infrastructure.dtos.TransactionIncoming;
import com.example.transactionenrichment.infrastructure.dtos.TransactionOutgoing;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.time.Instant;
import java.util.Optional;

import static com.example.transactionenrichment.transactions.TransactionEntity.*;

@RequiredArgsConstructor
class TransactionService {

    private final TransactionRepository repository;

    Optional<TransactionOutgoing> findById(String transactionId) {
        return repository.findById(transactionId).map(TransactionEntity::dto);
    }

    TransactionOutgoing enrichTransaction(TransactionIncoming transactionEvent) {
        TransactionEntity transactionEntity = enrichedTransactionCreatedFrom(transactionEvent);
        return repository.save(transactionEntity).dto();
    }


    public Page<TransactionOutgoing> findTransactionsBetweenDates(Instant from, Instant to, Pageable pageable) {
        return repository.findBySaleDateBetween(from, to, pageable).map(TransactionEntity::dto);
    }
}
