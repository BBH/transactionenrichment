package com.example.transactionenrichment.transactions;


import com.example.transactionenrichment.infrastructure.dtos.TransactionIncoming;
import com.example.transactionenrichment.infrastructure.dtos.TransactionOutgoing;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.time.Instant;
import java.util.Optional;

@RequiredArgsConstructor
public class TransactionFacade {

    private final TransactionService service;

    public Optional<TransactionOutgoing> findById(String transactionId) {
        return service.findById(transactionId);
    }

    public TransactionOutgoing enrichTransactionWithSumOfPrices(TransactionIncoming transactionEvent) {
        return service.enrichTransaction(transactionEvent);
    }

    public Page<TransactionOutgoing> findTransactionsBetweenDates(Instant from, Instant to, Pageable pageable) {
        return service.findTransactionsBetweenDates(from, to, pageable);
    }
}
