package com.example.transactionenrichment.transactions;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class TransactionFacadeConfig {

    TransactionFacade createFacade() {
        TransactionRepository repository = new TransactionInMemoryRepository();
        return createFacade(repository);
    }

    @Bean
    TransactionFacade createFacade(@Qualifier("transactionRepository") TransactionRepository repository) {
        TransactionService service = new TransactionService(repository);
        return new TransactionFacade(service);
    }
}
