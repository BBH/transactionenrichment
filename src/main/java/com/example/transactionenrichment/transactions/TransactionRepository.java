package com.example.transactionenrichment.transactions;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.Repository;

import java.time.Instant;
import java.util.Optional;

interface TransactionRepository extends Repository<TransactionEntity, String> {

    TransactionEntity save(TransactionEntity transactionEntity);

    Optional<TransactionEntity> findById(String transactionId);

    Page<TransactionEntity> findBySaleDateBetween(Instant from, Instant to, Pageable pageable);

}
