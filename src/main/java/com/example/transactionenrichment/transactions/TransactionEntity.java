package com.example.transactionenrichment.transactions;

import com.example.transactionenrichment.infrastructure.dtos.TransactionIncoming;
import com.example.transactionenrichment.infrastructure.dtos.TransactionOutgoing;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.Fetch;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.Set;
import java.util.stream.Collectors;

import static javax.persistence.CascadeType.ALL;
import static lombok.AccessLevel.PRIVATE;
import static lombok.AccessLevel.PROTECTED;
import static org.hibernate.annotations.FetchMode.JOIN;

@Entity
@Getter
@FieldDefaults(level = PRIVATE)
@NoArgsConstructor(access = PROTECTED)
@AllArgsConstructor
class TransactionEntity {

    @Id
    String id;

    Instant saleDate;

    @Fetch(JOIN)
    @JoinColumn(name = "transactionId")
    @OneToMany(cascade = ALL, orphanRemoval = true)
    Set<ProductEntity> products;

    BigDecimal sumOfProductPrices;

    static TransactionEntity enrichedTransactionCreatedFrom(TransactionIncoming transactionEvent) {
        Set<ProductEntity> productSet = transactionEvent.getProducts()
                .stream()
                .map(product -> new ProductEntity(product.getName(), product.getPrice()))
                .collect(Collectors.toSet());
        return new TransactionEntity(transactionEvent.getId(), transactionEvent.getSaleDate(), productSet, evaluateSum(productSet));
    }

    private static BigDecimal evaluateSum(Set<ProductEntity> products) {
        return products.stream().map(ProductEntity::getPrice).reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    TransactionOutgoing dto() {
        return createDtoFrom(this);
    }

    private TransactionOutgoing createDtoFrom(TransactionEntity entity) {
        return TransactionOutgoing
                .builder()
                .id(entity.id)
                .saleDate(entity.saleDate)
                .products(entity.products.stream().map(ProductEntity::dto).collect(Collectors.toList()))
                .sumOfProductPrices(entity.sumOfProductPrices)
                .build();
    }
}
