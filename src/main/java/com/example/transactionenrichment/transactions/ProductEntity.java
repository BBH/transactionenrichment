package com.example.transactionenrichment.transactions;

import com.example.transactionenrichment.infrastructure.dtos.ProductDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.UUID;

import static lombok.AccessLevel.PRIVATE;
import static lombok.AccessLevel.PROTECTED;

@Entity
@Getter
@FieldDefaults(level = PRIVATE)
@NoArgsConstructor(access = PROTECTED)
class ProductEntity {

    @Id
    String id;

    String name;
    BigDecimal price;

    ProductEntity(String name, BigDecimal price) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.price = price;
    }

    ProductDto dto() {
        return createDtoFrom(this);
    }

    private ProductDto createDtoFrom(ProductEntity entity) {
        return ProductDto
                .builder()
                .name(entity.name)
                .price(entity.price)
                .build();
    }
}
