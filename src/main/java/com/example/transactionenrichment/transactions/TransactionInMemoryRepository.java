package com.example.transactionenrichment.transactions;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;

import static java.util.stream.Collectors.toList;

class TransactionInMemoryRepository implements TransactionRepository {

    private final ConcurrentHashMap<String, TransactionEntity> transactions = new ConcurrentHashMap<>();

    @Override
    public TransactionEntity save(TransactionEntity transactionEntity) {
        transactions.put(transactionEntity.getId(), transactionEntity);
        return transactionEntity;
    }

    @Override
    public Optional<TransactionEntity> findById(String transactionId) {
        return Optional.ofNullable(transactions.get(transactionId));
    }

    @Override
    public Page<TransactionEntity> findBySaleDateBetween(Instant from, Instant to, Pageable pageable) {
        Predicate<TransactionEntity> bySaleDateFromTo = transactionEntity
                -> transactionEntity.getSaleDate().isAfter(from.minus(1, ChronoUnit.MILLIS))
                && transactionEntity.getSaleDate().isBefore(to.plus(1, ChronoUnit.MILLIS));
        List<TransactionEntity> transactionEntities = findTransactionEntities(bySaleDateFromTo);
        PagedListHolder<TransactionEntity> transactionEntityPagedListHolder = getTransactionEntityPagedListHolder(pageable, transactionEntities);
        return new PageImpl<>(transactionEntityPagedListHolder.getPageList(), pageable, transactionEntityPagedListHolder.getNrOfElements());
    }

    private List<TransactionEntity> findTransactionEntities(Predicate<TransactionEntity> byPredicate) {
        return transactions
                .values()
                .stream()
                .filter(byPredicate)
                .collect(toList());
    }

    private PagedListHolder<TransactionEntity> getTransactionEntityPagedListHolder(Pageable pageable, List<TransactionEntity> transactionEntities) {
        PagedListHolder<TransactionEntity> transactionEntityPagedListHolder = new PagedListHolder<>(transactionEntities);
        int pageSize = pageable.isPaged() ? pageable.getPageSize() : transactionEntityPagedListHolder.getNrOfElements();
        int offset = pageable.isPaged() ? (int) pageable.getOffset() : 0;
        transactionEntityPagedListHolder.setPageSize(pageSize);
        transactionEntityPagedListHolder.setPage(offset);
        return transactionEntityPagedListHolder;
    }

}
