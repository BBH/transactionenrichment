package com.example.transactionenrichment;

public class Profiles {

    public static final String INTEGRATION_TEST = "integration-test";

    public static final String NOT_INTEGRATION_TEST = "!" + INTEGRATION_TEST;

}
