### Requirements
* java11
* docker
* docker-compose

### Run tests 
./gradlew test

### Create and run:
* ./gradlew clean build
* ./gradlew bootBuildImage
* docker-compose up
* create database "database_name", e.g. by create database database_name with owner postgres;
* create topic "transactions" on kafka
* send sample transaction events to kafka, e.g.
{"id": "transactionId",
"saleDate": "2022-01-17T18:00:00.00Z",
"products": [{"name": "product1","price": 2.22},
{"name": "product2","price": 1.5}]}

## How to use API: 

http://localhost:8080/transactions/{id} returns transaction with id (type String)

http://localhost:8080/transactions?page={Integer}&size={Integer}&dateFrom={Instant}&dateTo={Instant}

returns page of transactions with sale date from dateFrom to dateTo (included), all parameters are optional (when there are no parameters, it returns all transactions)

